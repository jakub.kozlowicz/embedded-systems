#include "stm32f407xx.h"

#if !defined(__SOFT_FP__) && defined(__ARM_FP)
#warning "FPU is not initialized, but the project is compiling for an FPU. Please initialize the FPU before use."
#endif

void clk_config(unsigned int freq) {
	/* Start HSI */
	RCC->CR |= RCC_CR_HSION_Msk;
	while (!(RCC->CR & RCC_CR_HSIRDY_Msk))
		;

	/* Start HSE */
	RCC->CR |= RCC_CR_HSEON_Msk;
	while (!(RCC->CR & RCC_CR_HSERDY_Msk))
		;

	/* Make HSI as system closk */
	RCC->CFGR |= RCC_CFGR_SW_HSI;
	while (RCC->CFGR & RCC_CFGR_SWS_HSI)
		;

	/* Switch off PLL */
	RCC->CR &= ~RCC_CR_PLLON_Msk;

	/* Configure PLLCFGR */
	RCC->PLLCFGR &= ~RCC_PLLCFGR_PLLP_Msk;

	if (freq > 432) {
		/* P = 0 */
		RCC->PLLCFGR |= 432 << RCC_PLLCFGR_PLLN_Pos;
	} else if (freq > 50) {
		/* P = 0 */
		RCC->PLLCFGR |= freq << RCC_PLLCFGR_PLLN_Pos;
	} else if (freq > 25) {
		/* P = 1 */
		RCC->PLLCFGR |= RCC_PLLCFGR_PLLP_0;
		RCC->PLLCFGR |= RCC_PLLCFGR_PLLP_1;

		RCC->PLLCFGR |= (2 * freq) << RCC_PLLCFGR_PLLN_Pos;
	} else {
		/* P = 8 */
		RCC->PLLCFGR |= RCC_PLLCFGR_PLLP_0;
		RCC->PLLCFGR |= RCC_PLLCFGR_PLLP_1;

		RCC->PLLCFGR |= (4 * freq) << RCC_PLLCFGR_PLLN_Pos;
	}

	/* Start PLL */
	RCC->CR |= RCC_CR_PLLON_Msk;
	while (!(RCC->CR & RCC_CR_PLLRDY_Msk))
		;

	/* Latency for memory */
	FLASH->ACR |= FLASH_ACR_DCEN;
	FLASH->ACR |= FLASH_ACR_ICEN;
	FLASH->ACR |= FLASH_ACR_PRFTEN;

	if (freq > 150) {
		/* 5 WS */
		FLASH->ACR &= ~FLASH_ACR_LATENCY_Msk;
		FLASH->ACR |= FLASH_ACR_LATENCY_5WS;
	} else if (freq > 120) {
		/* 4 WS */
		FLASH->ACR &= ~FLASH_ACR_LATENCY_Msk;
		FLASH->ACR |= FLASH_ACR_LATENCY_4WS;
	} else if (freq > 90) {
		/* 3 WS */
		FLASH->ACR &= ~FLASH_ACR_LATENCY_Msk;
		FLASH->ACR |= FLASH_ACR_LATENCY_3WS;
	} else if (freq > 60) {
		/* 2 WS */
		FLASH->ACR &= ~FLASH_ACR_LATENCY_Msk;
		FLASH->ACR |= FLASH_ACR_LATENCY_2WS;
	} else if (freq < 30) {
		/* 1 WS */
		FLASH->ACR &= ~FLASH_ACR_LATENCY_Msk;
		FLASH->ACR |= FLASH_ACR_LATENCY_1WS;
	} else {
		/* 0 WS */
		FLASH->ACR &= ~FLASH_ACR_LATENCY_Msk;
		FLASH->ACR |= FLASH_ACR_LATENCY_0WS;
	}

	/* Make PLL as system clock */
	RCC->CFGR |= RCC_CFGR_SW_PLL;

	/* Stop HSI */
	RCC->CR &= ~RCC_CR_HSION_Msk;
}

void SysTick_Handler(void) {
	GPIOD->ODR ^= GPIO_ODR_OD15;
}

int main(void) {
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
	GPIOD->MODER |= GPIO_MODER_MODER15_0;

	// RCC->CR |= RCC_CR_HSEON_Msk;
	// while(!(RCC->CR & RCC_CR_HSERDY_Msk));
	//
	// RCC->CFGR |= RCC_CFGR_SWS_HSE;
	// while(!(RCC->CFGR & RCC_CFGR_SWS_HSE));

	clk_config(50);

	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk | SysTick_CTRL_TICKINT_Msk;
	SysTick->LOAD |= 2000000 - 1;

	while (1)
		;

	return 0;
}
