# Embedded systems

## Task A

Turn builtin LED on.

## Task B

Blink builtin LED using SysTick timer and polling for count flag.

## Task C

Blink builtin LED using SysTick timer and interrupts.

## Task D

Configure SysTick timer to use HSE (high speed external oscillator).

## Task E

Configure PLL clock as system clock with variable frequency.

## Task F

Configure timer 4 to manipulate builtin LED.
