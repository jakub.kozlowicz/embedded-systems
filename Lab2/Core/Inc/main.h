/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define SONAR_ECHO_Pin GPIO_PIN_5
#define SONAR_ECHO_GPIO_Port GPIOE
#define SONAR_EN_Pin GPIO_PIN_10
#define SONAR_EN_GPIO_Port GPIOE
#define SENSOR_EN_Pin GPIO_PIN_12
#define SENSOR_EN_GPIO_Port GPIOE
#define LED_DECOR_R_Pin GPIO_PIN_13
#define LED_DECOR_R_GPIO_Port GPIOE
#define LED_DECOR_G_Pin GPIO_PIN_14
#define LED_DECOR_G_GPIO_Port GPIOE
#define LED_DECOR_B_Pin GPIO_PIN_15
#define LED_DECOR_B_GPIO_Port GPIOE
#define SENSOR_6_Pin GPIO_PIN_8
#define SENSOR_6_GPIO_Port GPIOD
#define SENSOR_7_Pin GPIO_PIN_9
#define SENSOR_7_GPIO_Port GPIOD
#define SENSOR_8_Pin GPIO_PIN_10
#define SENSOR_8_GPIO_Port GPIOD
#define SENSOR_9_Pin GPIO_PIN_11
#define SENSOR_9_GPIO_Port GPIOD
#define MOTOR_R_PH_Pin GPIO_PIN_7
#define MOTOR_R_PH_GPIO_Port GPIOC
#define MOTOR_L_PH_Pin GPIO_PIN_9
#define MOTOR_L_PH_GPIO_Port GPIOC
#define SENSOR_1_Pin GPIO_PIN_3
#define SENSOR_1_GPIO_Port GPIOD
#define SENSOR_2_Pin GPIO_PIN_4
#define SENSOR_2_GPIO_Port GPIOD
#define SENSOR_3_Pin GPIO_PIN_5
#define SENSOR_3_GPIO_Port GPIOD
#define SENSOR_4_Pin GPIO_PIN_6
#define SENSOR_4_GPIO_Port GPIOD
#define SENSOR_5_Pin GPIO_PIN_7
#define SENSOR_5_GPIO_Port GPIOD
#define SONAR_TRIG_Pin GPIO_PIN_8
#define SONAR_TRIG_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
