#include "stm32f407xx.h"

#if !defined(__SOFT_FP__) && defined(__ARM_FP)
#warning "FPU is not initialized, but the project is compiling for an FPU. Please initialize the FPU before use."
#endif

void tim4_config(void) {
	/* Enable NVIC interrupts */
	NVIC_EnableIRQ(TIM4_IRQn);

	/* Enable clock for Timer 4*/
	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;

	/* Set prescalers for system clock, peripherals and timers */
	RCC->CFGR |= RCC_CFGR_HPRE_DIV1;
	RCC->CFGR |= RCC_CFGR_PPRE1_DIV2;
	RCC->CFGR |= RCC_CFGR_PPRE2_DIV1;

	/*
	 * Set prescaler and auto-reload register according to formula
	 * f = (TIM_CLK)/((PSC + 1) * (ARR + 1))
	 */
	TIM4->PSC = (4000 - 1);
	TIM4->ARR = (21000 - 1);

	/* Update interrupt trigger */
	TIM4->DIER |= TIM_DIER_UIE;

	/* Enable counter */
	TIM4->CR1 |= TIM_CR1_CEN;
}

void clk_config(unsigned int freq) {
	/* Start HSI */
	RCC->CR |= RCC_CR_HSION_Msk;
	while (!(RCC->CR & RCC_CR_HSIRDY_Msk))
		;

	/* Start HSE */
	RCC->CR |= RCC_CR_HSEON_Msk;
	while (!(RCC->CR & RCC_CR_HSERDY_Msk))
		;

	/* Make HSI as system clock */
	RCC->CFGR |= RCC_CFGR_SW_HSI;
	while (RCC->CFGR & RCC_CFGR_SWS_HSI)
		;

	/* Switch off PLL */
	RCC->CR &= ~RCC_CR_PLLON_Msk;

	/* Set HSE as PLL source */
	RCC->PLLCFGR |= RCC_PLLCFGR_PLLSRC_HSE;

	/* Configure M, N, P dividers */
	RCC->PLLCFGR &= ~RCC_PLLCFGR_PLLM;
	RCC->PLLCFGR |= RCC_PLLCFGR_PLLM_2;

	RCC->PLLCFGR &= ~RCC_PLLCFGR_PLLN;
	RCC->PLLCFGR &= ~RCC_PLLCFGR_PLLP;
	if (freq > 432) {
		/* P = 2 */
		RCC->PLLCFGR |= 432 << RCC_PLLCFGR_PLLN_Pos;
	} else if (freq > 50) {
		/* P = 2 */
		RCC->PLLCFGR |= freq << RCC_PLLCFGR_PLLN_Pos;
	} else if (freq > 25) {
		/* P = 4 */
		RCC->PLLCFGR |= RCC_PLLCFGR_PLLP_0;

		RCC->PLLCFGR |= (2 * freq) << RCC_PLLCFGR_PLLN_Pos;
	} else {
		/* P = 8 */
		RCC->PLLCFGR |= RCC_PLLCFGR_PLLP_0;
		RCC->PLLCFGR |= RCC_PLLCFGR_PLLP_1;

		RCC->PLLCFGR |= (4 * freq) << RCC_PLLCFGR_PLLN_Pos;
	}

	/* Start PLL */
	RCC->CR |= RCC_CR_PLLON_Msk;
	while (!(RCC->CR & RCC_CR_PLLRDY_Msk))
		;

	/* Latency for memory */
	FLASH->ACR |= FLASH_ACR_DCEN;
	FLASH->ACR |= FLASH_ACR_ICEN;
	FLASH->ACR |= FLASH_ACR_PRFTEN;
	FLASH->ACR &= ~FLASH_ACR_LATENCY_Msk;

	if (freq > 150) {
		/* 5 WS */
		FLASH->ACR |= FLASH_ACR_LATENCY_5WS;
	} else if (freq > 120) {
		/* 4 WS */
		FLASH->ACR |= FLASH_ACR_LATENCY_4WS;
	} else if (freq > 90) {
		/* 3 WS */
		FLASH->ACR |= FLASH_ACR_LATENCY_3WS;
	} else if (freq > 60) {
		/* 2 WS */
		FLASH->ACR |= FLASH_ACR_LATENCY_2WS;
	} else if (freq < 30) {
		/* 1 WS */
		FLASH->ACR |= FLASH_ACR_LATENCY_1WS;
	} else {
		/* 0 WS */
		FLASH->ACR |= FLASH_ACR_LATENCY_0WS;
	}

	/* Make PLL as system clock */
	RCC->CFGR |= RCC_CFGR_SW_PLL;
	while (!(RCC->CFGR & RCC_CFGR_SWS_PLL))
		;

	/* Stop HSI */
	RCC->CR &= ~RCC_CR_HSION_Msk;
}

void TIM4_IRQHandler(void) {
	/* Clear interrupt flag */
	TIM4->SR &= ~TIM_SR_UIF;
	GPIOD->ODR ^= GPIO_ODR_OD15;
}

int main(void) {
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
	GPIOD->MODER |= GPIO_MODER_MODER15_0;

	clk_config(84);
	tim4_config();

	while (1)
		;

	return 0;
}
