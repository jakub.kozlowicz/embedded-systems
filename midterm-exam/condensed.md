# 1. Embedded system – what it is, main elements.

Embedded system is a system designed to perform/control specific functions in a
larger design. Often with real time computing constraints (i.e. with
predictable response). Embedded system is typically a combination of software
and hardware parts enclosed in bigger device. The basis for defining an
embedded system is the function it performs and the place of its installation.
Because Embedded System has defined functionality, thus it can be heavily
optimized in order to:

- minimize total cost,
- maximize performance,
- maximize performance/cost ratio.

Main elements of the embedded system are:

- CPU (MCU based, DSC/DSP based, FPGA based),
- hardware (computing platforms, OEM platforms, bare metal),
- software (operating system, predefined libraries, bare metal).

# 2. What is a microprocessor? Main elements of microprocessors.

Microprocessor is an integrated circuit that contains all the functions of a
central processing unit of a computer. It is a programmable device which
accepts digital data and process this data according to the instructions saved
in the program memory. This device provides result as output. Operates on
symbols represented in binary numeral system.

Main elements of the microprocessor:

- **Arithmetic logic unit** -- fundamental component of a microprocessor,
  responsible for performing arithmetic and logical operations on binary data.
  It is a digital circuit that performs operations such that addition,
  subtraction, comparison and logical operations. Some units also provides
  multiplication and even more rarely division.
- **Status Register** -- hardware register which records the condition of the
  CPU as a result of arithmetic, logical or command operations. The purpose of
  the Status Register is to hold information about the most recently performed
  ALU operation, control the enabling and disabling of interrupts and set the
  CPU operating mode. Used sometimes for multiple word data manipulation. The
  status register contains various flags and control bits. Flags in the
  register are modified by different assembly code instructions.
- **Stack pointer** -- pointer that ALWAYS points to the top of the stack. In
  microprocessors, a stack pointer is a memory unit in the address register
  that stores the address of the top of the stack. The main concepts of using
  stack is to store temporary data, to store local variables, and to store
  return address after calling subroutine or interrupt handler.
- **Program counter** -- contains the address of the currently executing
  instruction. The program counter is automatically advanced to point to the
  next instruction. In the case of a jump, subroutine call a new value will
  simply be loaded into the program counter in order to cause a jump.
- **Instruction decoder** -- In a computer system, the Instruction Decoder is a
  component of the CPU (Central Processing Unit) that is responsible for
  decoding instructions that are fetched from memory. The Instruction Decoder
  receives an instruction in the form of a binary code and translates it into a
  series of signals that control the CPU's operation. In order to speed up
  execution of the instructions some MCU implements pipelining (`Fetch ->
Decode -> Execute`). This allow to fetch next instruction while decoding
  current one. This has some disadvantages like data or control hazards.
- **Registers / Scratch Memory** -- type of high-speed memory storage within a
  computer's central processing unit (CPU). Registers are used to hold data
  that the CPU needs to access frequently and quickly, such as data being
  processed, intermediate results of operations, and memory addresses.
- **Interrupts** -- method of creating a temporary halt during program
  execution and allows peripheral devices to access the microprocessor. The
  microprocessor responds to that interrupt with an ISR (Interrupt Service
  Routine), which is a short program to instruct the microprocessor on how to
  handle the interrupt. The main advantages of using interrupts are increase
  performance, simplicity of implementing multitasking, easy way for a
  communication between peripherals and CPU.

# 3. Program flow in microprocessors. Memory map.

A program is a set of instructions arranged in the specific sequence to do the
specific task. It tells the microprocessor what it has to do. The MCU executes
instructions one after the other. To keep track of the instruction, the program
counter is used. Instruction decoder loads and decodes instructions from the
program flash and later program counter points to the current instruction being
executed. In case of interrupts the program counter stack is used. When a
subroutine is called, it is necessary to change the PC value. The previous
operation address goes to the stack so that the program can return to this
place after the subprogram has been executed. **Program execution starts from
the address pointed by a special RESET pointer.**

There are two main memory maps -- split and unified. The unified memory map has
one memory for data and instruction in comparison where split memory map has
two different memories for data and instructions. Having two memories proceeds
to higher usage of the memory compared to the unified solution. Also unified
memory is easier to program. Example of split memory map is Harvard
architecture. Unified memory map can be seen in Von Neumann architecture.

# 4. Von Neumann vs Harvard architecture. CISC vs RISC.

**Von Neumann** architecture has one memory space for program code and data.
Furthermore it has single bus for CPU, memory, I/O and devices. This require
sequential access to program code and data. On the other hand we can use memory
more flexibly. Sequential access and only one bus can lead to slower
performance due to bus competition.

On the other hand **Harvard** architecture has split memory space for program
code and data. This requires more attention from the programming point of view,
as well as uses more memory. Also Harvard architecture has multiple buses to
access data, program code or peripherals. In Harvard architecture it is
possible to fetch instruction and data in parallel which can lead to increased
performance.

**CISC** (Complex Instruction Set Computer) is a set of instruction that has
large number of complex instructions. Some of the instruction could take
multiple clock cycles to complete. Due to complexity of the instructions
compilers often have difficulty to optimize code. The MCU in CISC architecture
is controlled by the microcode and has small number of registers.

**RISC** (Reduced Instruction Set Computer) is a set of instruction that has
few very simple instructions. Many of the instructions executes in only one
clock cycle. Due to simplicity of the operations the compilers can optimize
code so much more compared to the CISC architecture. In this architecture MCU
is controlled by hardware and has large number of auxiliary registers.

# 5. Classifications of microcontrollers. SISD vs SIMD vs MIMD.

**SISD** (Single Instruction Single Data) is a very popular but not so
efficient computer architecture where there is only one stream of instructions
and one stream of data. Most microprocessors use this architecture.

**SIMD** (Single Instruction Multiple Data) is a computer architecture used in
supercomputers, vector coprocessors and DSP devices. It is efficient in
parallel processing data.

**MIMD** (Multiple Instruction Multiple Data) is a computer architecture used
in computation networks. This is the most efficient parallel processing
architecture.

# 6. 8-bit microcontrollers. Comparison of architectures of AVR and PIC microcontrollers.

8-bit microcontrollers are still on the market and in some many projects
because of their simplicity and price. Furthermore lots of projects already use
those microcontrollers so there is huge amount of legacy. The dimensions and
power consumption are also big advantages.

The AVR microcontrollers are based on RISC architecture. Most of processors
have clocks up to 20MHz and 1MIPS/MHz.

The PIC microcontrollers are based on RISC and modified Harvard architecture.
They also implements two-stage pipelining. Instructions in the PIC
microcontrollers have single word length.

# 7. Important electrical parameters of microcontrollers.

- **Maximum ratings** -- microcontrollers have to be viewed as a complicated
  device, sensitive to many factors. Each device have their own **Absolute
  Maximum Ratings** that must be viewed very carefully. The values found in
  that table **MUST NOT be exceeded**.
- **Operating temperature** -- microcontrollers as other electronics can only
  operates in specific temperature range. Usually it is from -40&deg;C up to
  130&deg;C. Microcontrollers are usually **NOT protected** against
  **overheating**.
- **Maximum operating voltage** -- maximum value of the voltage that can be
  connected to the microcontroller pin. For microcontrollers with multiple
  voltage inputs this is the maximum value defined for each input separately.
- **Input/Output voltage range** -- maximum value of the voltage that can be
  connected to any pin of the microcontroller. This value usually corresponds
  to the power supply voltage and may stay within some limits (-0.5V to
  VCC+0.5V). This voltage can also be changed when the CLAMP CURRENT is used
  properly.
- **Clamp current** -- the maximum value of the current that can be sink or
  output can source without damage. With the combination of series resistor the
  input voltage can be extremely increased (also negative voltage possible).
  Value of current outputted or sourced are usually in mA range.
- **Power supply** -- powering the microcontroller is a delicate task. The most
  problematic microcontrollers to power are those with analog processing part.
  Most of the microcontrollers have more than one power supply input. There are
  two major types of power supply circuits -- linear and switching. Each have
  their advantages and disadvantages, but linear is more used in low-power
  circuits and switching in high-power. To change value of voltage the DC-DC
  converters are used. To step up voltage use boost converter and to step down
  -- buck. There is also combination of two mentioned above -- buck-boost.
- **Current consumption** -- all of microcontrollers need some juice in order
  to work. Nowadays there is pressure to reduce power consumption for all
  devices. Often microcontrollers offers different modes to reduce power
  consumption. To measure different devices the mA/MIPS rating is introduced.
  It tells how much current it is required for million instructions executed
  per second. This rating closely depends on MCU clocking.
- **I/O port parameters** -- as pins of the microcontrollers can operate in
  different modes there is need to define theirs electrical parameters. For
  example pins can have different levels of the digital logic. For most
  microcontrollers low limit can have some negative voltage and slightly more
  over supply voltage for input mode.

# 8. Microcontrollers clocking.

- **External Crystal Oscillator** -- connected between XTAL inputs. Requires
  changing of physical component to change frequency. Fragile to the
  capacitance of C1 and C2 but also of the PCB board.
- **Low Frequency Crystal Oscillator** -- optimized for low frequency crystals
  (mainly 32.768 kHz -- RTC). Stability of this clocking is similar to the
  crystal oscillator.
- **External RC Oscillator** -- very simple and cheap solution. Has the very
  low stability and is limited to few MHz. To change frequency it is required
  to change physical components. _The most stupid thing Budzyń knows_.
- **Internal RC Oscillator** -- very simple and cheap option. Ease of frequency
  regulation -- only software register change necessary. Has very low
  stability.
- **External Clock** -- the most advanced solution of all mentioned. Requires
  only one but very complicated physical component -- clock generator. In order
  to change frequency whole component need to be replaced. Has high to ultra
  high stability.
- **Internal PLL** -- requires external clock to proper work. Easy to change
  frequency with software registers. Has wide range of available frequency. The
  stability of the outputted clock usually does not exceed 1 ppm.

# 9. Types of reset.

The reset circuit is necessary to sometimes start microcontroller from the
beginning. All I/O registers are set to initial values during reset phase. The
program start execution from the reset vector. Important note -- the reset
circuit does not require clocking as it is asynchronous.

There are different types of reset:

- **Power-on reset** -- MCU is reset when the power supply voltage drops below
  power-on reset threshold value (VPOT).
- **External reset** -- MCU is reset when low level is present on the RESET
  line for longer than minimum pulse length.
- **Watchdog reset** -- MCU is reset when the watchdog timer period expires and
  the watchdog feature is enabled.
- **Brown-out reset** -- MCU is reset when the supply voltage is below
  brown-out threshold value.

# 10. ARM7TDMI operation modes and registers.

The ARM7TDMI is based on Von Neumann architecture with 3 stage pipelining on
32-bit RISC processor with low power consumption. It can operate in seven
different modes. The first three are connected to operations on data -- 8-bit
(byte), 16-bit (halfword), 32-bit (word). The other four corresponds to the
functionalities -- Thumb, Debug, Multiplier, and Interrupts.

The ARM7TDMI has total of 37 registers -- 31 general-purpose 32-bit registers
and 6 status registers. All of registers cannot be seen at once -- the MCU
state and operating mode tells which registers are available to the programmer.

The ARM state allows to see 16 general-purpose registers and one or two status
registers. The first twelve register are for arithmetic operations and data
movement. The rest four are used for stack pointer, link register, and program
counter.

The THUMB state is a subset of the ARM state. It limits the first twelve
registers to only eight. The rest stays the same.

# 11. ARM instruction set vs Thumb instruction set.

The THUMB instruction set is subset of ARM set. Instructions in the THUMB set
has equivalent in ARM set. The ARM set use more memory but it is also 32-bit
compared to 16-bit instructions in the THUMB set. Both sets operates on 32-bit
data. The THUMB set has some limitation on conditional execution with also
reduced number of general-purpose registers. There are no Thumb coprocessor
instructions, semaphore instructions, or instructions to access the CPSR or
SPSR.

# 12. Cortex family members.

There are three categories for the Cortex cores:

- **A** (application processors) –- High-end processors for mobile computing,
  smart phone, servers, etc. These processors run at higher clock frequency
  (over 1Ghz), and support Memory Management Unit (MMU), which is required for
  full creature OS. (A5, A7, A17, A32, A35, A53, A73, A76)
- **R** (real-time processors) –- These are very high performance processors
  for real time applications such as hard disk controllers or automotive
  powertrain. Most of these processors do not have MMU, and usually have Memory
  Protection Unit(MPU), cache and other memory features designed for industrial
  applications. They can run at fairly high clock frequency (200 Mhz to >1 Ghz)
  and have very low response latency. (R4, R5, R7, R8, R52)
- **M** (microcontrollers) -- These processors are usually designed to have a
  much lower silicon area, and much higher energy efficiency. Typically, they
  have short pipelines, and usually lower maximum frequency. The Cortex-M
  processor family is designed to be very easy to use. (M0, M0+, M1, M3, M4,
  M7)

# 13. Main elements of Cortex M0 and M0+.

This two microprocessors are the simples of the Cortex M family. The M0 is the
smallest and simplest version of ARM cores with 3-stage pipelining. It is
suitable for analog and mixed signal devices and delivers 32-bit performance at
16-bit or 8-bit price. The M0 core have upward compatibility with the M3 core.
The interrupts have 16 cycle delay in execution.

On the other hand the M0+ have reduced power consumption and improved
performance due to 2-stage pipelining. It implements fast I/O (one clock
cycle), MPU and MTB.

# 14. Cortex M3 vs Cortex-M4(F).

- **Architecture** The Cortex-M3 is based on the Armv7-M architecture, while
  the Cortex-M4(F) is based on the Armv7E-M architecture. The Armv7E-M
  architecture provides additional features such as DSP instructions, optional
  floating-point unit (FPU), and support for larger memories.
- **DSP extensions** The Cortex-M4(F) includes digital signal processing (DSP)
  extensions, such as single-cycle 16/32-bit MAC, single-cycle dual 16-bit MAC,
  8/16-bit SIMD arithmetic, and hardware divide (2-12 cycles). These extensions
  can accelerate mathematically intensive operations commonly found in digital
  signal processing applications.
- **Floating Point Unit (FPU)** The Cortex-M4(F) includes an optional
  single-precision FPU, which provides hardware support for floating-point
  operations according to the IEEE 754 standard. This enables faster and more
  accurate floating-point calculations, which are commonly used in scientific,
  engineering, and signal processing applications.
- **Performance** Due to its DSP extensions and optional FPU, the Cortex-M4(F)
  is generally faster and more powerful than the Cortex-M3. It can execute more
  instructions per clock cycle and perform more mathematically intensive
  operations per second.
- **Cost** The Cortex-M4(F) is typically more expensive than the Cortex-M3 due
  to its additional features and performance capabilities.

# 15. Cortex-M4F vs Cortex-M7.

- **Floating Point Unit** Both the Cortex-M4F and Cortex-M7 feature a hardware
  Floating Point Unit (FPU). However, the Cortex-M7's FPU supports both
  single-precision and double-precision floating-point operations, while the
  Cortex-M4F's FPU only supports single-precision floating-point operations.
- **I-cache/D-cache** A cache memory that stores frequently accessed
  instructions/data from the main memory, providing faster access to the
  processor.
- **Instruction Tightly-Coupled Memory (I-TCM)/(D-TCM)** A fast memory that is
  physically close to the processor core and is used to store critical,
  time-sensitive code or instructions/data.
- **Advanced eXtensible Interface (AXI-M)** A high-performance bus protocol
  used for on-chip communication between IP cores and memory interfaces,
  supporting high bandwidth and low latency.

# 16. What is special in Cortex M23, M33 and M35P?

Those types of cores differs from other Ms in introduction of TrustZone.
TrustZone provides hardware implemented isolation between trusted and untrusted
resources of these cores while maintaining efficiency. The secure part is used
for storing sensitive data, such as cryptographic keys, and executing secure
software, such as security protocols and secure boot code. The non-secure part
can access the secure section through a secure gateway, but only if authorized.
The TrustZone technology provides a hardware-enforced boundary between the
secure and non-secure worlds, which helps to protect against security threats,
such as malware, hacking, and unauthorized access. However those
implementations are not perfect because there is possibility of hacking the
password if the peaks of voltage and current are analyzed. To prevent the last
hacking action, the M35P implements anti-tampering layer.

# 17. NVIC -- features.

NVIC (Nested Vectored Interrupt Controller) – on-chip controller that provides
fast and low latency response to interrupt-driven events in ARM Cortex-M MCUs.
It can support up to 240 external interrupts with many levels of priority, that
can be dynamically changed. Interrupt sources with higher priority levels are
handled before interrupt sources with lower priority levels.

By assigning different priorities to each interrupt, the NVIC can support
Nested Interrupts automatically without any software intervention.

- **Tail chaining** -- If another exception is pending when an ISR exits, the
  processor does not restore all saved registers from the stack and instead
  moves on to the next ISR.
- **Stack pop pre-emption** -- If another exception occurs during the
  unstacking process of an exception, the processor abandons the stack Pop and
  services the new interrupt immediately.
- **Late arrival** -- If a higher priority interrupt arrives during the
  stacking of a lower priority interrupt, the processor fetches a new vector
  address and processes the higher priority interrupt first.

# 18. CMSIS –- major functionality.

CMSIS (Cortex Microcontroller Software Interface Standard) is a standardized
software interface that enables software developers to write portable code for
ARM Cortex-M microcontrollers. It is designed to simplify software development
and promote code reuse across different Cortex-M-based microcontrollers from
different manufacturers. It includes a set of header files, libraries, and code
examples that conform to the CMSIS standard, making it easier to write, test,
and maintain software code.

Components that can be found in CMSIS package:

- **Core** -- Provides a set of core access functions for Cortex-M processors,
  including interrupt handling, system timer, and power management.
- **DSP** -- Provides a collection of optimized digital signal processing (DSP)
  functions for Cortex-M processors, including filtering, FFT, and matrix
  operations.
- **RTOS** -- Provides a standardized interface for Real-Time Operating Systems
  (RTOS) on Cortex-M processors, including thread management, synchronization,
  and communication primitives.
- **Driver** -- Provides a standardized interface for microcontroller
  peripheral drivers, including UARTs, SPIs, and I2Cs.
- **Pack** -- Provides a standardized format for software component
  distribution, including software libraries, device support files, and example
  projects.
- **SVD** -- Provides a standardized format for describing the memory-mapped
  registers of microcontroller peripherals.
- **NN** -- Provides a collection of optimized neural network functions for
  Cortex-M processors.

# 19. Compare SPI and I2C.

1. **Communication speed** -- SPI supports higher speeds compared to I2C. SPI
   typically operates at speeds up to 10 MHz or higher, while I2C operates at a
   maximum speed of 3.4 MHz.
2. **Bus Architecture** -- In SPI, each device requires a dedicated line
   (except daisy chaining), with one master device and one or more slave devices
   connected. In contrast, I2C uses a two-wire bus, with all devices connected to
   the same bus, and each device has a unique address.
3. **Transmission** -- Mode In SPI, data can transmitted in full duplex mode,
   allowing for simultaneous data transmission in both directions. In contrast,
   I2C typically operates in half-duplex mode, meaning that data can only be
   transmitted in one direction at a time.
4. **Signal Voltage** -- I2C uses a voltage level of 3.3V or 5V, while SPI can
   operate at higher voltage levels, typically 3.3V, 5V or even higher.
5. **Pinout** -- SPI requires at least 4 pins (SCLK, MISO, MOSI, SS) to be
   used, whereas I2C can operates using only 2 pins (SCL, SDA).
6. **Complexity** -- I2C is more complex bus in both software and hardware
   part. SPI on the other hand requires less hardware blocks. Programming
   communication with SPI bus have the same complexity to the I2C (at least when
   using HAL).

# 20. UART.

UART (Universal Asynchronous Receiver/Transmitter) is a type of serial
communication protocol used for transmitting data between two devices. It is
commonly used in microcontrollers and other embedded systems. UART uses two
communication lines: a transmit line (TX) and a receive line (RX). It can
operate in both synchronous and asynchronous modes, with the second one being
more common.

Features:

- One of the most popular serial interfaces
- Serial, two-wire interface
- Used for internal and external communication
- Original versions (e.g. RS-232C) require many control lines (DB9, DB25)
- Two state logic used, but not TTL!
- Asynchronous data transfer. No clock transmitted!
- Full duplex transmission
- Point – to – point transmission
- Throughput up to hundreds of kb/s

# 21. What is the use of single wire interfaces?

Singe wire interfaces are useful because of its simplicity. They require only
one wire to transfer data from device to device. The most popular interface in
that form is OneWire from Dallas Semiconductor. This protocol is used for
communication with a variety of devices, including temperature sensors, memory
devices, and real-time clocks. It uses a unique device ID to address each
device on the network, allowing for multiple devices to be connected to a
single wire.

Features:

- Serial, one-wire interface
- Communication wire can also be used for delivering power supply!
- Used for internal communication but on longer distances
- Master-slave architecture
- Asynchronous data transfer. Data transfer controlled by the master
- Half-duplex transmission
- Point–to–point transmission
- Data rate 15.4kb/s (standard) or 125kb/s (overdrive)

# 22. What makes CAN transfer so reliable?

1. **Error Detection and Correction** -- CAN uses a sophisticated error
   detection and correction mechanism that ensures the integrity of the data
   transmitted on the bus. This mechanism includes CRC (Cyclic Redundancy Check)
   codes and ACK (Acknowledgement) bits that are used to detect and correct
   errors. Also CRC and ACK takes more than half of the one communication frame.
2. **Message Prioritization** -- CAN allows for message prioritization, which
   means that messages with higher priority can be given precedence over messages
   with lower prior- ity. This is particularly important in time-critical
   applications, such as automotive systems, where certain messages need to be
   processed quickly.
3. **Differential Signaling** -- CAN uses differential signaling, which means
   that the signal is transmitted across two wires, with one wire carrying the
   inverted signal of the other. This allows for noise cancellation and improves
   the overall signal quality.
4. **Bus Arbitration** -- CAN uses a unique arbitration mechanism that ensures
   that only one device on the bus is transmitting at any given time. This helps
   to prevent collisions and ensures that all devices on the bus have an equal
   opportunity to transmit data.
5. **Robustness** -- CAN is designed to be robust and can operate in harsh
   environments. It can tolerate a wide range of temperatures and can operate over
   long distances without significant signal degradation.
