# Embedded Systems midterm exam

## 1. Embedded system – what it is, main elements.

Embedded system is a system designed to perform/control specific functions in a
larger design. Often with real time computing constraints (i.e. with
predictable response). Embedded system is typically a combination of software
and hardware parts enclosed in bigger device. The basis for defining an
embedded system is the function it performs and the place of its installation.
Because Embedded System has defined functionality, thus it can be heavily
optimized in order to:

- Minimize total cost
- Maximize performance
- Maximize performance/cost ratio

### Embedded System CPUs

| MCU based | DSC/ DSP based | FPGA based |
| :-------: | :------------: | :--------: |
|   8-bit   |     16-bit     | Pure logic |
|  16-bit   |     32-bit     |  SoC type  |
|  32-bit   |     64-bit     |            |
|  64-bit   |                |            |

### Embedded System Hardware

| Computing platforms | OEM platforms | Bare metal |
| :-----------------: | :-----------: | :--------: |
|    Raspberry Pi     |     ESP32     | MCU based  |
|       Arduino       |    Nucleo     | DSC based  |
|      Discovery      | Mediatronics  | FPGA based |
|       64-bit        |               |   Hybrid   |

### Embedded System Software architecture

|         Operating system         |                    Predefined libraries                     |                     Bare metal                     |
| :------------------------------: | :---------------------------------------------------------: | :------------------------------------------------: |
|       Complex applications       | Decent balance of resources, performance and time to market |             Most lightweight solution              |
|      Multiuser programming       |                                                             | Real time and Cycle accurate applications possible |
| Difficulties in real time usages |                                                             |                                                    |
|         Resource hungry          |                                                             |                                                    |

### Embedded System Software languages

| Interpreted languages  |       Object oriented languages       |     Structural programming     |                Machine code                |
| :--------------------: | :-----------------------------------: | :----------------------------: | :----------------------------------------: |
|    Simple to learn     |        Human-like programming         | Perfect in simple applications | Very difficult coding and code maintenance |
| Abundance of libraries | Can be used in effective applications |         Very effective         |         Most effective final code          |
|      Ineffective       |                                       |                                |                                            |

## 2. What is a microprocessor? Main elements of microprocessors.

Microprocessor is an integrated circuit that contains all the functions of a
central processing unit of a computer.

**Microprocessor**:

- is a programmable device
- accepts digital input data
- processes the data according to instructions stored in memory
- provides results as output
- operates on symbols represented in binary numeral system

**Main elements**:

- Arithmetic Logic Unit
- Status Register
- Stack Pointer
- Program Counter
- Instruction Decoder
- Registers / Scratch Memory
- Interrupt subsystem

### Main elements of microprocessor

#### Arithmetic logic unit

An arithmetic-logic unit is the part of a central processing unit that carries
out arithmetic and logic operations on the operands in computer instruction
words. Some processors contain more than one AU -- for example, one for
fixed-point operations and another for floating-point operations. In computer
systems, floating-point computations are sometimes done by a floating-point
unit (FPU) on a separate chip called a numeric coprocessor.

![alu_symbol](./images/alu_symbol.png "ALU Symbol")

Width of the **A**, **B**, and **R** defines if the microprocessor is
**8**, **16**, **32** or **64** bits.

Usually **ALU** offers following operations

- addition,
- subtraction,
- comparison,
- logical operations,
- multiplication,
- _division_ (some units **NEVER** offers).

#### Status register

The Status Register is a hardware register which records the condition of the
CPU as a result of arithmetic, logical or command operations. The purpose of
the Status Register is to hold information about the most recently performed
ALU operation, control the enabling and disabling of interrupts and set the CPU
operating mode. Used sometimes for multiple word data manipulation. The status
register contains various flags and control bits. Flags in the register are
modified by different assembly code instructions.

#### Stack pointer

Stack Pointer is a pointer that **ALWAYS** points to the top of the stack. In
microprocessors, a stack pointer is a memory unit in the address register that
stores the address of the top of the stack.

**The Stack is mainly used**:

- for storing temporary data,
- for storing local variables,
- for storing return addresses after interrupts and subroutine calls.

#### Program counter

Program Counter (PC) is a special register holding the address of the
instruction currently being executed. Points to the program flash (ones and
zeros). When microprocessor is after reset the program counter points to the
first starting instruction. Program counter is automatically advanced to point
to the next instruction.

#### Instruction decoder

A hardware element of the microprocessor that fetches instruction from the program memory in binary format. Later decodes instruction and send it to ALU or proper subsystem. After decoding, the instruction is being executed.

To speed up this process, pipelining is involved.

```mermaid
graph LR
A[Fetch] --> B[Decode] --> C[Execute]
```

At the same time, three actions are executed using different data/program buses.

#### Registers / Scratch memory

Each CPU contains number of **universals registers**. They are **much faster**
rather then external memory. Mainly used to as **temporary storage for data**
(accessing intermediate results, data being currently processed). Registers are
**specially addressed**, so it is **easier/faster** to access them.

In some CPUs instead of registers a memory is used.

#### Interrupts

Interrupt is the method of creating a **temporary halt** during program
execution and allows peripheral devices to access the microprocessor. The
microprocessor responds to that interrupt with an **ISR** (Interrupt Service
Routine), which is a short program to instruct the microprocessor on how to
**handle the interrupt**.

Main advantages to use interrupts:

- it is the simplest multitasking
- they simplify programming model (event triggered)
- it is an easy way for a communication between peripherals and CPU
- they **increase performance** of the MCU

```mermaid
graph TD;
    A[Normal application flow] --> B[Interrupt handler]
    B --> C[Normal application fow]

    subgraph Program flow
        A
        C
    end
    subgraph Interrupts
        B
    end
```

## 3. Program flow in microprocessors. Memory map.

### Program flow

A program is a set of instructions arranged in the specific sequence to do the
specific task. It tells the microprocessor what it has to do. The MCU executes
instructions one after the other. To keep track of the instruction, the program
counter is used. Instruction decoder loads and decodes instructions from the
program flash and later program counter points to the current instruction being
executed. In case of interrupts the program counter stack is used. When a
subroutine is called, it is necessary to change the PC value. The previous
operation address goes to the stack so that the program can return to this
place after the subprogram has been executed.

- The program control logic and program- address generation logic work together to provide proper program flow.
- Normally, the flow of a program is sequential:
  - the CPU executes instructions at consecutive program-memory addresses,
  - discontinuities are caused by branches, function calls or interrupts.
- Program execution starts from the address pointed by a special **RESET** pointer.
- Instructions are in a machine code i.e. stream of 8, 16 or 32 bit long values.

### Memory map

|                            |              Split memory map               |                Unified memory map                 |
| :------------------------: | :-----------------------------------------: | :-----------------------------------------------: |
|    Memory architecture     |  Memory for data and instructions is split  |       Same memory for instructions and data       |
|        Memory usage        |           Higher usage of memory            | Less memory needed compared to split architecture |
|    Example architecture    |            Harvard architecture             |             Von Neumann architecture              |
| Programming implementation | Requires special attention from programmers |        Simpler from software point of view        |

## 4. Von Neumann vs Harvard architecture. CISC vs RISC.

### CISC vs RISC

|                        | CISC (Complex Instructions Set Computer) |    RISC (Reduced Instructions Set Computer)    |
| :--------------------: | :--------------------------------------: | :--------------------------------------------: |
| Number of instructions |       Large number of instructions       | Reduced number of instructions (even below 30) |
|      Clock cycles      | Some instructions need many clock cycles | Most instructions performed in one clock cycle |
| Types of instructions  |    Existence of complex instructions     |       Instructions simple or very simple       |
|   Code optimization    | Difficult for compilers to optimize code |     Easier for compilers to optimize code      |
|    Register number     |        Small number of registers         |      Large number of auxiliary registers       |
|      MCU control       |       MCU controlled by microcode        |           MCU controlled by hardware           |
|    Addressing modes    |     Large number of addressing modes     |        Small number of addressing modes        |

### Von Neumann vs Harvard

|                     |                 Von Neumann                 |                  Harvard                   |
| :-----------------: | :-----------------------------------------: | :----------------------------------------: |
|  Bus architecture   |        Unified data and program bus         |       Separate data and program bus        |
| Memory architecture |  Single memory space for data and program   | Separate memory space for data and program |
|     Bus access      | Possible competitions on bus data transfers | Separate buses so impossible competitions  |
|    Memory access    | Program and data access in sequential mode  |  Program and data access in parallel mode  |

## 5. Classifications of microcontrollers. SISD vs SIMD vs MIMD

|                SISD                |                     MISD                     |                        SIMD                         |                 MIMD                  |
| :--------------------------------: | :------------------------------------------: | :-------------------------------------------------: | :-----------------------------------: |
| Single data and instruction stream | Multiple instruction and single data streams |    Single instruction and multiple data streams     | Multiple data and instruction streams |
|            Very popular            |                 Rarely used                  | Used in supercomputers, vector coprocessors and DSP |     Used in computation networks      |
|         Reduced efficiency         |              Parallel operation              |            Efficient parallel processing            |     Efficient parallel processing     |
|     ![SISD](./images/SISD.svg)     |          ![MISD](./images/MISD.svg)          |             ![SIMD](./images/SIMD.svg)              |      ![MIMD](./images/MIMD.svg)       |

**Most of the microprocessors use SISD architecture.**

## 6. 8-bit microcontrollers. Comparison of architectures of AVR and PIC microcontrollers

<!-- TODO: Write this section -->

## 7. Important electrical parameters of microcontrollers

**Important parameters**:

- Maximum ratings
- Power supply:
  - Voltage
  - Current consumption
- Clocking
- Reset
- I/O ports parameters

### Maximum ratings

Every device have their limits. Embedded devices and especially microprocessors
have to be viewed as fragile electronics. Every device have their **_Absolute
Maximum Ratings_** that must be viewed carefully. The values found in ratings
table **MUST NOT** be exceeded by any fraction.

#### Operating temperature

- Usually from -40&deg;C to 130&deg;C
- Differs from chip vendor to chip vendor
- Usually different temperature versions available
- Thermal resistance should be taken into consideration in order to avoid problems with overheating
- Microcontrollers are usually **NOT protected** against overheating!

#### Maximum operating voltage

- Maximum value of voltage that can be connected to power supply input of the microcontrollers
- For microcontrollers with multiple power supplies inputs, the maximum operating voltage is defined for each input separately

#### Input / output voltage range:

- Maximum value of voltage that can be connected to any pin of the microcontroller
- The value is usually limited by the actual power supply value and may stay within some limits (i.e. -0.5V to VCC+0.5V)
- The input-output voltage range can be **extended** when the **CLAMP CURRENT** parameter is used **properly**

#### Clamp Current:

- The maximum current that the input can sink or the output can source without any damage
- Usually in mA range
- If a series resistor is used then even very high or negative voltages can be directly connected to the microcontroller
- Usable in ESD protection

### Power supply

- Delivering power to microcontrollers is usually a delicate task
- Most of microcontrollers have more than 1 power supply input
- Each power supply input have usually different requirements
- The most problematic for proper power supply are microcontrollers with analog part

|                     |                         Linear Power Supply                         |                             Switching Power Supply                              |
| :-----------------: | :-----------------------------------------------------------------: | :-----------------------------------------------------------------------------: |
|    How it works?    | A transformer reduces AC input and converts it to clean DC voltage. | Uses pulse width modification (PWM) to efficiently regulate the output voltage. |
|        Pros         |                 Quiet, Great for lower power output                 |              High efficiency, Flexible applications, Compact size               |
|        Cons         |                Larger, heavier size, Low efficiency                 |                              High frequency noise                               |
| Common Applications |                 Communication, Medical, Laboratory                  |               Manufacturing, Mobile stations, Aviation, Shipboard               |

#### DC-DC Converters

1. **Buck converter**

   In another words -- _step-down converter_. A DC-to-DC converter which steps
   down voltage (while stepping up current) from its input (supply) to its
   output (load).

2. **Boost converter**

   In another words -- _step-up converter_. A DC-to-DC power converter that
   steps up voltage (while stepping down current) from its input (supply) to
   its output (load)

3. **Buck-boost converter**

   A type of DC-to-DC converter that has an output voltage magnitude that is
   either greater than or less than the input voltage magnitude.

4. **Cuk converter**

   Type of Buck-Boost converter but with galvanic isolation.

5. **SEPIC converter**

   Type of Buck-Boost converter but with feature which keeps output voltage at
   the same level, regardless of changes of input voltage.

#### Current consumption

- The requirements on power consumption reduction are nowadays very high
- The active supply current is in the range of 1mA/MIPS
- The active supply current depends on the main clock frequency
- Microcontrollers offer many supply modes in order to reduce power consumption

### I/O ports parameters

- **Functionalities**
  - input - usually has some Schmitt triggering with hysteresis (we need hysteresis to get rid of noises)
  - output - two capacitors push-pull, open drain or disabled
  - analog - can be input and output (bidirectional)
  - alternate - digital, pin is connected directly and it's functionality depend on peripherals
- **Parameters**
  - output/input high/low voltage - we can calculate internal resistance with it (impedance)

#### Registers

- **ODR** (output data registers) - outputs the data
- **IDR** (input data register) - reading the data (state of the pin) - read only
- **DDR** (data direction register) - setting pins as inputs or outputs (input=0, output=1)
- **CR1**, **CR2** (configuration registers) - deciding about additional
  features (floating and pull-up with or without interrupt, pull-up or open
  drain output, fast mode or not)

## 8. Microcontrollers clocking

### External Crystal Oscillator

- An external crystal oscillator is connected between XTAL inputs
- Modifying clock frequency requires change of the physical object (crystal)
- Fragile to capacitance (C1, C2 and capacitance of PCB board have effect on result frequency)
- Clock stability can be high (1-10ppm)

### Low Frequency Crystal Oscillator

- Optimized for low frequency crystals -- mainly the 32.768 kHz
- Similar stability like standard Crystal Oscillator

### External RC Oscillator

- Very simple and cheap option
- Just two discreet components necessary -- Limited to a few MHz range
- Ease of frequency regulation
- Very low stability (100-1000 ppm)
- _The most stupid thing Budzyń knows_

### Internal RC Oscillator

- Very simple and cheap option
- Limited to a few MHz range
- Ease of frequency regulation – only software register change necessary
- Possibility of frequency calibration
- Very low stability (100-1000 ppm)

### External Clock

- The most expensive option
- Just one (but complicated!) component necessary – the clock generator
- Very wide frequency range available (even above 100MHz)
- No possibility of frequency regulation
- High to Ultra high stability (0.000001 - 10 ppm)!!!

### Internal PLL

- Requires external clock or external oscillator
- Wide frequency range available
- Ease of frequency regulation via software register
- Stability depends on the clock source and the jitter of PLL. Usually not better than 1ppm

## 9. Types of resets

- During Reset:
  - all I/O Registers are set to their initial values
  - the program starts execution from the Reset Vector
- The reset circuitry does not require any clock source to be running (it is asynchronous)

### Power-on Reset

The MCU is reset when the supply voltage is below the Power-on Reset threshold (VPOT)

![power-on-reset](./images/power-on-reset.png)

### External Reset

The MCU is reset when a low level is present on the RESET pin for longer than the minimum pulse length

![external-reset](./images/external-reset.png)

### Watchdog Reset

The MCU is reset when the Watchdog Timer period expires and the Watchdog is enabled

![watchdog-reset](./images/watchdog-reset.png)

### Brown-out Reset

The MCU is reset when the supply voltage VCC is below the Brown-out Reset threshold

![brown-out-reset](./images/brown-out-reset.png)

## 10. ARM7TDMI operation modes and registers

### Main features

- 32-bit RISC processor with low power consumption
- Von Neumann architecture
- 3-stage pipelining
  - Fetch
  - Decode
  - Execute
- Two instruction sets
  - 32-bit ARM
  - 16-bit Thumb
- Seven work modes

**TDMI** consists of:

- Thumb
- Debug
- Multiplier
- Interrupts

#### Operation on data

- **8-bit** (byte)
- **16-bit** (halfword)
- **32-bit** (word)

### Registers

- 37 registers:
  - 31 general purpose registers
  - 6 status registers
- Available number depends on the operation mode and processor state
- R15 is always the program counter
- R13 is usually the stack pointer (by convention)
- R14 is sometimes used as Link Register (in Branch with Link BL instruction)

#### Status registers

- The ARM7TDMI processor contains a CPSR (Current Program Status Register) and five SPSRs (Saved Program Status Register) for exception handlers to use.
- The program status registers:
  - hold information about the most recently performed ALU operation
  - control the enabling and disabling of interrupts -- set the processor operating mode.

#### Registers types

|      Mode       | Identifier |                    Description                     |
| :-------------: | :--------: | :------------------------------------------------: |
|      User       |    usr     |         Usual ARM program execution state          |
| Fast interrupts |    fiq     |    Supports a data transfer or channel process     |
|   Interrupts    |    irq     |    Used for general-purpose interrupt handling     |
|   Supervisor    |    svc     |      Protected mode for the operating system       |
|      Abort      |    abt     | Entered after a data or instruction Prefetch Abort |
|     System      |    sys     |   Privileged user mode for the operating system    |
|    Undefined    |    und     | Entered when an undefined instruction is executed  |

## 11. ARM instruction set vs Thumb instruction set

|                                     ARM                                     |                      Thumb                       |
| :-------------------------------------------------------------------------: | :----------------------------------------------: |
| ARM(Acorn Risc Machine) is a 32-bit RISC instruction set architecture (ISA) |              16-bit instruction set              |
|     Common data and instruction buses for simpler versions(von Neumann)     |        Commands occupy small memory space        |
|        Split data and instruction buses for faster versions(Harvard)        | Only some commands can be executed conditionally |
|                                 Pipelining                                  |  Each instruction has its parent in the ARM set  |
|                          Occupy large memory space                          |          Only R0-R7 can be freely used           |
|               Each instruction can be executed conditionally                |                                                  |
|                            Five addressing modes                            |                                                  |
|                    Each addressing node has few options                     |                                                  |
|                                    Fast                                     |                                                  |

**Thumb2 contains ARM and Thumb instruction sets!**

## 12. Cortex family members

There are three categories for the Cortex cores:

- **A** (application processors) -- High-end processors for mobile computing,
  smart phone, servers, etc. These processors run at higher clock frequency
  (over 1Ghz), and support Memory Management Unit (MMU), which is required for
  full creature OS.
- **R** (real-time processors) -- These are very high performance processors
  for real time applications such as hard disk controllers or automotive
  powertrain. Most of these processors do not have MMU, and usually have Memory
  Protection Unit(MPU), cache and other memory features designed for industrial
  applications. They can run at fairly high clock frequency (200 Mhz to >1 Ghz)
  and have very low response latency.
- **M** (microcontrollers) -- These processors are usually designed to have a
  much lower silicon area, and much higher energy efficiency. Typically, they
  have short pipelines, and usually lower maximum frequency. The Cortex-M
  processor family is designed to be very easy to use.

|                |   Microcontrollers and deeply embedded   | Real time processors | Application processors(with mmu support linux, MS mobile OS) |
| :------------: | :--------------------------------------: | :------------------: | :----------------------------------------------------------: |
|     32-bit     | M0, M0+, M1 (FPGA), M3, M4, M7, M23, M33 | R4, R5, R7, R8, R52  |                A5, A7, A8, A9, A12, A17, A32                 |
|     64-bit     |                    -                     |          -           |            A35, A53, A57, A72, A73, A55, A75, A76            |
| Security cores |  M23, M33, M35p, M55 (Machine learning)  |          -           |                              -                               |

## 13. Main elements of Cortex M0 and M0+

|                          Cortex-M0                           |                             Cortex-M0+                              |
| :----------------------------------------------------------: | :-----------------------------------------------------------------: |
|   The simplest version of ARM cores with 3-stage pipeline    | Reduced energy consumption and increased performance over Cortex-M0 |
| Performance Efficiency 2.33 CoreMarks/MHz and 1.27 DMIPS/MHz |    Performance Efficiency 2.46 CoreMarks/MHz and 1.35 DMIPS/MHz     |
|   Very power saving version of ARM cores – only 5.3 uW/MHz   |  The most power saving version of ARM cores – only 3.8 uW/MHz !!!   |
|             Upward compatibility with Cortex-M3              |                         Two-stage pipeline                          |
|                       Only 12000 gates                       |                NMI with 1 to 32 physical interrupts                 |
|               Only 56 C-optimized instructions               |              Single cycle 32x32 multiply instructions               |
|             NMI with 1 to 32 physical interrupts             |    Optional 8 region MPU with sub regions and background region     |
|           Single cycle 32x32 multiply instructions           |                                                                     |
|             Interrupt execution delay: 16 cycles             |                                                                     |
|             ![cortex-m0](./images/cortex-m0.png)             |               ![cortex-m0+](./images/cortex-m0+.png)                |

### Differences

|                Feature                 |     Cortex-M0     |             Cortex-M0+              |
| :------------------------------------: | :---------------: | :---------------------------------: |
|                Pipeline                |    Three-stage    |              Two-stage              |
|         Performance Efficiency         | 2.33 CoreMark/MHz |          2.46 CoreMark/MHz          |
|           Memory Protection            |   Not available   | Has optional Memory protection Unit |
|        Relocatable vector-table        | Does not support  |              Supports               |
| Unprivileged/privileged mode execution | Does not support  |              Supports               |
|              Energy usage              |    5.3 uW/MHz     |             3.8 uW/MHz              |

## 14. Cortex-M3 vs Cortex-M4(F) vs Cortex-M7

|                                       Cortex-M3                                       |                                            Cortex-M4(F)                                             |                                    Cortex-M7                                     |
| :-----------------------------------------------------------------------------------: | :-------------------------------------------------------------------------------------------------: | :------------------------------------------------------------------------------: |
| The industry-leading 32-bit processor for highly deterministic real-time applications |                         Destined for low power digital signal applications                          | The highest performance member of the energy-efficient Cortex-M processor family |
|             Performance Efficiency: 3.34 CoreMark/MHz and 1.89 DMIPS/MHz              | Performance Efficiency: 3.40 CoreMark/MHz and without FPU: 1.91 DMIPS/MHz, With FPU: 1.95 DMIPS/MHz |            Performance Efficiency: 5 CoreMark/MHz and: 3.23 DMIPS/MHz            |
|                           Low power consumption (11 uW/MHz)                           |                                            12.26 uW/MHz                                             |                           Dynamic power only 33 uW/MHz                           |
|                   Up to 240 interrupt sources with 8-256 priorities                   |                                     Integrated 32b CPU and DSP                                      |        Optional 8 or 16 region MPU with sub regions and background region        |
|                       Integrated Bit Instructions & Bit Banding                       |                                      Single precision FPU unit                                      |               6 stage superscalar pipeline with branch prediction                |
|                  Single cycle 32x32bit multiply; 2-12 cycle division                  |                                  Other features like in Cortex-M3                                   |                       Single or Double precision FPU unit                        |
|                     Three stage pipelining with branch prediction                     |                             DSP extension -- MAC and SIMD instructions                              |                                   DSP features                                   |
|             Optional 8 region MPU with sub regions and background region              |                               3 stage pipeline with branch prediction                               |                         I and D-caches up to 64 kB each                          |
|                          Max speed: up to 275 MHz /340 DMIPS                          |                                 Max speed: up to 300 MHz /375 DMIPS                                 |                          ITCM and DTCM up to 16 MB each                          |
|                         ![cortex-m3](./images/cortex-m3.png)                          |                                ![cortex-m4](./images/cortex-m4.png)                                 |                       ![cortex-m7](./images/cortex-m7.png)                       |

## 16. What is special in Cortex M23, M33 and M35P

Those types of cores differs from other Ms in introduction of TrustZone.
TrustZone provides hardware implemented isolation between trusted and untrusted
resources of these cores while maintaining efficiency. However those
implementations are not perfect because there is possibility of hacking the
password if the peaks of voltage and current are analyzed. To prevent the last
hacking action, the M35P implements anti-tampering layer.

## 17. NVIC - features

NVIC (Nested Vectored Interrupt Controller) -- on-chip controller that provides
fast and low latency response to interrupt-driven events in ARM Cortex-M MCUs.
It can support up to 240 external interrupts with many levels of priority, that
can be dynamically changed.

By assigning different priorities to each interrupt, the NVIC can support
Nested Interrupts automatically without any software intervention.

![nvic](./images/nvic.jpg)

### Tail chaining

If another exception is pending when an ISR exits, the processor does not
restore all saved registers from the stack and instead moves on to the next
ISR.

![nvic-tail-chaining](./images/tail-chain.jpg)

### Stack pop pre-emption

If another exception occurs during the unstacking process of an exception, the
processor abandons the stack Pop and services the new interrupt immediately.

![nvic-stack-pop-pre-emption](./images/staco-pop-pre-emption.jpg)

### Late arrival

If a higher priority interrupt arrives during the stacking of a lower priority
interrupt, the processor fetches a new vector address and processes the higher
priority interrupt first.

![nvic-late-arrival](./images/late-arrival.jpg)

## 18. CMSIS -- major functionality

CMSIS (Cortex Microcontroller Software Interface Standard):

- enables consistent device support and simple software interfaces to the processor and its peripherals,
- simplifying software reuse,
- reducing the learning curve for microcontroller developers
- reducing the time to market for new devices

### Components

1. **Core**
   - API for the Cortex-M processor core and peripherals.
   - Includes system startup, processor core access, and peripheral definitions.
   - Provides at standardized interface for over 3900 different devices
   - Implements the basic run-time system for a Cortex-M device
   1. **Hardware Abstraction Layer** (HAL) for Cortex-M processor registers with standardized definitions for the SysTick, NVIC, System Control Block registers, MPU registers, FPU registers, and core access functions.
   2. **System exception** names to interface to system exceptions without having compatibility issues.
   3. **Methods to organize header** files that makes it easy to learn new Cortex-M microcontroller products and improve software portability. This includes naming conventions for device-specific interrupts.
   4. **Methods for system initialization** to be used by each MCU vendor. For example, the standardized SystemInit() function is essential for configuring the clock system of the device.
   5. **Intrinsic functions** used to generate CPU instructions that are not supported by standard C functions.
2. **Driver**
   - Generic peripheral interfaces for middleware and application code
   - Ready-to use CMSIS-Driver interfaces are today available for many microcontroller families and avoid cumbersome and time consuming driver porting
3. **DSP**
   - Fast implementation of digital signal processing.
   - CMSIS-DSP library is a rich collection of DSP functions that Arm has optimized for the various Cortex-M processor cores.
   - Is widely used in the industry and enables also optimized C code generation from various third-party tools.
   - Over 60 Functions for various data types: fix-point (fractional q7, q15, q31) and single precision floating-point (32-bit).
   - Different categories:
     - Basic math functions
     - Fast math functions
     - Complex math functions -- Filters
     - Matrix functions:
       - Matrix Addition
       - Complex Matrix Multiplication
       - Matrix Initialization
       - Matrix Inverse
       - Matrix Multiplication
       - Matrix Scale
       - Matrix Subtraction
       - Matrix Transpose
     - Transforms
     - Motor control functions -- Statistical functions
     - Support functions
     - Interpolation functions
4. **NN**
   - Collection of efficient neural network kernels developed to maximize the performance and minimize the memory footprint of neural networks on Cortex-M
   - Specific categories:
     - Neural Network Convolution Functions
     - Neural Network Activation Functions
     - Fully-connected Layer Functions
     - Neural Network Pooling Functions
     - Softmax Functions
     - Neural Network Support Functions
5. **RTOS**
   - Common API for Real-Time operating systems.
   - Provides a standardized programming interface that is portable to many RTOS and enables therefore software templates, middleware, libraries, and other components that can work across supported the RTOS systems.
6. **DAP**
   - Connectivity to low-cost evaluation hardware.
   - Standardized interface to the Cortex Debug Access Port (DAP) and is used by many starter kits and supported by various debuggers.
7. **SVD**
   - Consistent view to device and peripherals.
   - For every supported microcontroller, debuggers can provide detailed views to the device peripherals that display the current register state.
   - Enable these views, and ensure that the debugger view matches the actual implementation of the device peripherals.
   - Developed and maintained by silicon vendors.
8. **Packs**
   - Easy access to reusable software components.
   - Software components are easily selectable, and any dependencies on other software are highlighted.

## 19. Compare SPI and I2C

### SPI

1. **Features**
   - One of the most popular serial interfaces
   - Serial, four-wire interface
   - Used for internal communication
   - Master-slave architecture
   - Two state TTL logic used!
   - Fully synchronous data transfer. Clock controlled by the master!
   - Full duplex transmission
   - Point–to–point transmission
   - Throughput up to tens of Mb/s
2. **Connections**
   - MISO -- master input slave output
   - MOSI -- master output slave input
   - SCLK -- serial clock
   - SS -- slave select
3. **Communication flow**
   - Both in Master and in Slave there are implemented shift registers (8-bits), SIPO and PISO type
   - Zata are exchanged with the SCK signal
   - After 8 (16) clock cycles the data transfer is finished
4. **Multiple devices**
   - Data bus
     - Multiple slave select pins
     - Same SCK, MISO, MOSI
     - One device selected at a time, rest act as listeners on common buses
   - Daisy chain
     - One slave select
     - MISO and MOSI daisy chained
     - Devices must support this feature

#### SPI in STM32F4

1. **Main features**
   - Full-duplex synchronous transfers on three lines
   - Simplex synchronous transfers on two lines with or without a bidirectional data line
   - 8- or 16-bit transfer frame format selection
   - Master or slave operation
   - Multimaster mode capability
   - Programmable clock polarity and phase
   - Programmable data order with MSB-first or LSB-first shifting
   - Hardware CRC feature for reliable communication
   - 1-byte transmission and reception buffer with DMA capability: Tx and Rx requests
2. **Half-duplex operation**
   - The SPI is capable of operating in half-duplex mode in 2 configurations:
     - 1 clock and 1 bidirectional data wire
     - 1 clock and 1 data wire (receive-only or transmit- only)
3. **Main registers**
   - **SPI_DR** -- 16b data register split into 2 buffers - one for writing (Transmit Buffer) and another one for reading (Receive buffer)
   - **SPI_SR** -- status register
   - **SPI_CR1** -- control register 1
   - **SPI_CR2** -- control register 2
4. **SPI communication using DMA**
   - SPI can be configure to operate at throughput exceeding 10Mb/s
   - To facilitate the transfers, the SPI features a DMA capability implementing a simple request/acknowledge protocol
   - In transmission, a DMA request is issued each time TXE is set to 1. The DMA then writes to the SPI_DR register.
   - In reception, a DMA request is issued each time RXNE is set to 1. The DMA then reads the SPI_DR register
5. **Interrupts**
   <!-- - TODO: Implement table with interrupts -->

### I2C

1. **Features**
   - Serial, two-wire interface
   - Used for internal communication
   - Master-slave architecture
   - Two state TTL logic used
   - Fully synchronous data transfer. Clock controlled by the master
   - Half-duplex transmission
   - Point-to–point transmission
   - Data rate 400kb/s (3.4Mb/s in HS mode)
   - Transmission over two lines
   - Both lines are open-drain type
   - Both lines are pulled-up
   - Each device checks, if there is no collision on the line
   - Data transfer can be initiated only by the master
2. **Addressing**
   - Each device has its own unique address
   - Address is 7-bit long
   - 8-th bit defines if there will be a read (1) or write (0) operation
3. **Special modes**
   - In the first protocol specifications (1982) the max speed was set to 100 kb/s
   - Nex the Fast Mode was introduced with the maximum speed of 400 kb/s
   - In 2006 the Fast Mode Plus was defined with maximum speed of 1Mb/s
   - With additional logic the, so called, Highspeed Mode can be implemented with maximum speed of 3.4 Mb/s

#### I2C in STM32F4

1. **Main features**
   - Multimaster capability: the same interface can act as Master or Slave
   - I2C Master features:
     - Clock generation
     - Start and Stop generation
   - I2C Slave features:
     - Programmable I2C Address detection
     - Dual Addressing Capability to acknowledge 2 slave addresses
     - Stop bit detection
   - Generation and detection of 7-bit/10-bit addressing
   - Supports different communication speeds: • Standard Speed (up to 100 kHz)
     - Fast Speed (up to 400 kHz)
   - Programmable digital noise filter
   - Optional clock stretching
   - 1-byte buffer with DMA capability
2. **Modes of operation**
   - Slave transmitter
   - Slave receiver
   - Master transmitter
   - Master receiver
   - By default the module operates in Slave mode
   - The interface automatically switches from slave to Master:
     - after it generates a START condition
   - The interface automatically switches from master to slave:
     - if an arbitration loss
     - a Stop generation occurs, allowing multimaster capability
3. **DMA**
   - DMA requests (when enabled) are generated only for data transfer
   - DMA requests are generated:
     - by Data Register becoming empty in transmission • Data Register becoming full in reception
4. **Interrupts**
   <!-- - TODO: Implement table with interrupts -->

#### I2C in STM32F4

1. **Main registers**
   - **I2C_CCR** -- Clock control register
   - **I2C_DR** --data register
   - **I2C_OAR1** -- Own address register
   - **I2C_SR1** -- status register 1
   - **I2C_SR2** -- status register 2
   - **I2C_CR1** -- control register 1
   - **I2C_CR2** -- control register 2

### Differences

1. **Communication speed**

   SPI supports higher speeds compared to I2C. SPI typically operates at speeds
   up to 10 MHz or higher, while I2C operates at a maximum speed of 3.4 MHz.

2. **Bus Architecture**

   In SPI, each device requires a dedicated line (except daisy chaining), with
   one master device and one or more slave devices connected. In contrast, I2C
   uses a two-wire bus, with all devices connected to the same bus, and each
   device has a unique address.

3. **Transmission Mode**

   In SPI, data can transmitted in full duplex mode, allowing for simultaneous
   data transmission in both directions. In contrast, I2C typically operates in
   half-duplex mode, meaning that data can only be transmitted in one direction
   at a time.

4. **Signal Voltage**

   I2C uses a voltage level of 3.3V or 5V, while SPI can operate at higher
   voltage levels, typically 3.3V, 5V or even higher.

5. **Pinouts**

   SPI requires at least 4 pins (SCLK, MISO, MOSI, SS) to be used, whereas I2C can operates
   using only 2 pins (SCL, SDA).

6. **Complexity**

   I2C is more complex bus in both software and hardware part. SPI on the other hand
   requires less hardware blocks. Programming communication with SPI bus have the same
   complexity to the I2C (at least when using HAL).

## 20. UART

**Features**:

- One of the most popular serial interfaces
- Serial, two-wire interface
- Used for internal and external communication
- Original versions (e.g. RS-232C) require many control lines (DB9, DB25)
- Two state logic used, but not TTL!
- Asynchronous data transfer. **No clock transmitted!**
- Full duplex transmission
- Point – to – point transmission
- Throughput up to hundreds of kb/s

### USART in STM32F4

- Full duplex, asynchronous or synchronous communications
- Fractional baud rate generator systems - Common programmable transmit and receive baud rate
- Programmable data word length (8 or 9 bits)
- Configurable stop bits -- support for 1 or 2 stop bits
- Transmitter clock output for synchronous transmission
- Single-wire half-duplex communication
- Configurable multibuffer communication using DMA (direct memory access)
  - Buffering of received/transmitted bytes in reserved SRAM using centralized DMA
- For asynchronous mode only two pins necessary:
  - RX: Receive Data Input
  - TX: Transmit Data Output
- Additional pin necessary for synchrounous mode:
  - SCLK: Transmitter clock output. This pin outputs the transmitter data clock for synchronous transmission
- Pins required in Hardware flow control mode:
  - nCTS: Clear To Send blocks the data transmission at the end of the current transfer when high
  - nRTS: Request to send indicates that the USART is ready to receive a data (when low).
- Important registers
  - **USART_SR** -- status register
  - **USART_DR** -- data register (8-bit value)
  - **USART_BRR** -- baud rate register
  - **USART_CR1** -- control register
  - **USART_CR2** -- control register
  - **USART_CR3** -- control register
- Parity control
  - Parity control -- generation of parity bit in transmission and parity checking in reception
  - Can be enabled by setting the PCE bit in the USART_CR1 register
  - Parity can be Even or Odd
- Synchronous mode
  - Full duplex, clock controlled mode used for fast data transfer to slave devices
- Half-duplex operation:
  - the TX and RX lines are internally connected
  - the RX pin is no longer used
- Hardware flow control:
  - It is possible to control the serial data flow between 2 devices by using the nCTS input and the nRTS output - the TX pin is always released when no data is transmitted. Thus, it acts as a standard I/O in idle or in reception.
  - Request To Send -- RTS flow control
    - nRTS is asserted (tied low) as long as the USART receiver is ready to receive a new data
  - Clear To Send - CTS flow control
    - the transmitter checks the nCTS input before transmitting the next frame
- Multiprocessor communication
  - There is a possibility of performing multiprocessor communication with the USART (several USARTs connected in a network)
  - One of the USARTs is a master -- its TX output is connected to the RX input of the other USARTs
  - The outputs of slaves are ANDed and connected to the RX of the master
  - Each processor has each own address (soft
    support)
- USART Interrupts
  <!--  TODO: Complete with table of interrupts -->

## 21. What is the use of single wire interfaces?

Singe wire interfaces are useful because of its simplicity. They require only
one wire to transfer data from device to device. The most popular interface in
that form is OneWire from Dallas Semiconductor.

### OneWire

1. **Features**
   - Serial, one-wire interface
   - Communication wire can also be used for **delivering power supply!**
   - Used for internal communication but on longer distances
   - Master-slave architecture
   - Asynchronous data transfer. Data transfer controlled by the master
   - Half-duplex transmission
   - Point–to–point transmission
   - Data rate 15.4kb/s (standard) or 125kb/s (overdrive)

## 22. What makes CAN transfer so reliable?

1. **Error Detection and Correction**

   CAN uses a sophisticated error detection and correction mechanism that
   ensures the integrity of the data transmitted on the bus. This mechanism
   includes CRC (Cyclic Redundancy Check) codes and ACK (Acknowledgement) bits
   that are used to detect and correct errors. Also CRC and ACK takes more than
   half of the one communication frame.

2. **Message Prioritization**

   CAN allows for message prioritization, which means that messages with higher
   priority can be given precedence over messages with lower priority. This is
   particularly important in time-critical applications, such as automotive
   systems, where certain messages need to be processed quickly.

3. **Differential Signaling**

   CAN uses differential signaling, which means that the signal is transmitted
   across two wires, with one wire carrying the inverted signal of the other. This
   allows for noise cancellation and improves the overall signal quality.

4. **Bus Arbitration**

   CAN uses a unique arbitration mechanism that ensures that only one device on
   the bus is transmitting at any given time. This helps to prevent collisions
   and ensures that all devices on the bus have an equal opportunity to
   transmit data.

5. **Robustness**

   CAN is designed to be robust and can operate in harsh environments. It can
   tolerate a wide range of temperatures and can operate over long distances
   without significant signal degradation.
